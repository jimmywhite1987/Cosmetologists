# `Cosmetologists`

## Project Description

- The goal of this project is to use the dataset included to then filter out stylists according to geographical location, as well as Licensing being up to date.
- Then, using the filtered data, create a custom dashboard that can be filtered through and sorted.
- The dataset was provided by a local salon owner who is seeking new stylists for the company.
- ***This project is still in progress.

## Languages & Technology Used

- CSS
- HTML
- JavaScript
- Python
- Jupyter Notebook

## Installation

- Install Jupyter Notebook (pip install)

## Links
- [JWhiteAnalytics.com](https://jwhiteanalytics.com)
- [LinkedIn](https://www.linkedin.com/in/jimmywhite1987)
- [Full Resume](https://jwhiteanalytics.com/JWhite%20Resume.pdf)
- [GitLab](https://gitlab.com/jimmywhite1987)
